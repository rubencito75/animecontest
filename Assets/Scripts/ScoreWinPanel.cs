﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using XboxBigButton;

public class ScoreWinPanel : MonoBehaviour
{
    public Image ui_colorPlayer;
    public TMPro.TextMeshProUGUI ui_score;
    public Image ui_clockImage;
    public TMPro.TextMeshProUGUI ui_turns;

    public float initDelay;
    public float timeToUpdateScore;
    public float timeToUpdateTurns;

    public void ShowPanel(Controller c, int addScore, int addTurns, Action callback)
    {
        transform.parent.gameObject.SetActive(true);
        StartCoroutine(MainRoutine(c, addScore, addTurns, callback));
    }

    IEnumerator MainRoutine(Controller c, int addScore, int addTurns, Action callback)
    {
        var playerIndex = (int)c;
        ui_colorPlayer.color = GameManager.instance.hudScore.First(x => x.playerType == (GameStatus.PlayerType)c).UI_Color.color;
        var score = GameStatus.instance.score[playerIndex];

        ui_score.text = score.score.ToString();
        ui_turns.text = score.waitTurns.ToString();

        ui_clockImage.gameObject.SetActive(addTurns > 0);
        ui_turns.gameObject.SetActive(addTurns > 0);

        yield return new WaitForSeconds(initDelay);

        if (addScore != 0)
        {
            float tmpScore = score.score;
            float scoreInc = timeToUpdateScore != 0 ? (float)addScore / timeToUpdateScore : 10000 * Mathf.Sign(addScore);
            float trgScore = score.score + addScore;

            var min = Mathf.Min(score.score, trgScore);
            var max = Mathf.Max(score.score, trgScore);

            while (!Mathf.Approximately(tmpScore, trgScore))
            {
                yield return null;
                tmpScore = Mathf.Clamp(tmpScore + scoreInc * Time.deltaTime, min, max);
                ui_score.text = ((int)tmpScore).ToString();
            }
            ui_score.text = ((int)trgScore).ToString();
        }

        if (addTurns != 0)
        {
            float tmpScore = score.waitTurns;
            float scoreInc = timeToUpdateTurns != 0 ? (float)addTurns / timeToUpdateTurns : 10000 * Mathf.Sign(addScore);
            float trgScore = score.waitTurns + addTurns;

            var min = Mathf.Min(score.waitTurns, trgScore);
            var max = Mathf.Max(score.waitTurns, trgScore);

            while (!Mathf.Approximately(tmpScore, trgScore))
            {
                yield return null;
                tmpScore = Mathf.Clamp(tmpScore + scoreInc * Time.deltaTime, min, max);
                ui_turns.text = $"+{(int)tmpScore}";
            }
            ui_turns.text = $"+{(int)trgScore}";
        }

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));

        callback?.Invoke();

        transform.parent.gameObject.SetActive(false);
    }
}
