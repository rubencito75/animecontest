using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : MonoBehaviour
{
    public Image progressBar;

    public float progress
    {
        get => progressBar.fillAmount;
        set
        {
            progressBar.fillAmount = value;
        }
    }
}
