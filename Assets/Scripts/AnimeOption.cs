﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimeOption : MonoBehaviour
{
    [Header("UI Elements")]
    public TMPro.TextMeshProUGUI UI_indexText;
    public TMPro.TextMeshProUGUI UI_infoText;
    public Image UI_background;

    [Header("Parameters")]
    public int index;
    public string customIndex;
    public string text;
    public Color backgroundColor = Color.white;

    private void UpdateText()
    {
        if (UI_indexText)
            UI_indexText.text = string.IsNullOrEmpty(customIndex) ? string.Concat(index, ":") : string.Concat(customIndex, ":");
        if (UI_infoText)
            UI_infoText.text = text;
        if (UI_background)
            UI_background.color = backgroundColor;
    }

    private void Update()
    {
        UpdateText();
    }

    private void OnValidate()
    {
        UpdateText();
    }
}
