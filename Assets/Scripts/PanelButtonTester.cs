﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelButtonTester : MonoBehaviour
{

    private void OnEnable()
    {
        FxManager.PlaySound(10, true);
    }

    private void OnDisable()
    {
        FxManager.StopMusic();
    }

    private void Start()
    {
        FxManager.PlaySound(10, true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
            gameObject.SetActive(false);
    }
}
