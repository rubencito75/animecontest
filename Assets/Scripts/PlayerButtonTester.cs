﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XboxBigButton;

public class PlayerButtonTester : MonoBehaviour
{
    public GameStatus.PlayerType player;

    public Image ui_bigButton;
    public Text ui_bigButtonText;
    public Image ui_buttonA;
    public Image ui_buttonB;

    public Color colorBigButton;
    public Color colorBigButtonPressed;
    public Color colorBigButtonText;
    public Color colorBigButtonTextPressed;
    public Color colorA;
    public Color colorAPressed;
    public Color colorB;
    public Color colorBPressed;

    private void Update()
    {
        var c = (Controller)player;
        ui_bigButton.color = XboxBigButtonInput.HasPressed(c, Buttons.BigButton) ? colorBigButtonPressed : colorBigButton;
        ui_bigButtonText.color = XboxBigButtonInput.HasPressed(c, Buttons.BigButton) ? colorBigButtonTextPressed : colorBigButtonText;
        ui_buttonA.color = XboxBigButtonInput.HasPressed(c, Buttons.A) ? colorAPressed : colorA;
        ui_buttonB.color = XboxBigButtonInput.HasPressed(c, Buttons.B) ? colorBPressed : colorB;
    }
}
