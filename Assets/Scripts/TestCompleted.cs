﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCompleted : MonoBehaviour
{
    public int countResolved;
    public int green;
    public int red;
    public int blue;
    public int yellow;

    private void OnEnable()
    {
        for (int i = 0; i < countResolved; i++)
            GameStatus.instance.resolved[i] = true;

        GameStatus.instance.playerGreen.score = green;
        GameStatus.instance.playerRed.score = red;
        GameStatus.instance.playerBlue.score = blue;
        GameStatus.instance.playerYellow.score = yellow;
    }
}
