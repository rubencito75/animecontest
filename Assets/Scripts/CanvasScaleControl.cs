﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScaleControl : MonoBehaviour
{
    CanvasScaler canvasScaler;

    private void OnEnable()
    {
        canvasScaler = GetComponent<CanvasScaler>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha9))
            canvasScaler.referenceResolution = new Vector2(canvasScaler.referenceResolution.x - 10, canvasScaler.referenceResolution.y);
        if (Input.GetKeyDown(KeyCode.Alpha0))
            canvasScaler.referenceResolution = new Vector2(canvasScaler.referenceResolution.x + 10, canvasScaler.referenceResolution.y);
    }
}
