﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using XboxBigButton;
using System.Linq;
using System.Text;
using System.IO;

public class GameManager : MonoBehaviour
{
    public TMPro.TextMeshProUGUI header;
    public GameClueMode clueMode;
    public GameResolveMode resolveMode;
    public ScoreWinPanel scoreWinPanel;
    public GameAnimeScoreMode animeScoreMode;
    public PanelButtonTester panelButtonTester;
    public ResultsPanel resultsPanel;
    public Transform animePanel;
    public Transform scorePanel;
    public Color colorResolved;
    public Color colorDefault;
    public bool upperCaseNames = true;

    [Header("Load game")]
    public LoadingPanel loadingPanel;
    public KeyCode keyLoadGame = KeyCode.Alpha1;
    public float timePressedToLoad = 5;
    private float _currentTimeLoadKeyPressed = 0;

    public static GameManager instance;

    internal AnimeOption[] animes;
    internal HUDPlayerScore[] hudScore;

    public void Start()
    {
        instance = this;
        animes = animePanel.GetComponentsInChildren<AnimeOption>();
        hudScore = scorePanel.GetComponentsInChildren<HUDPlayerScore>();
        clueMode.onFinishedMode += ClueMode_onFinishedMode;
        resolveMode.onFinishedMode += ResolveMode_onFinishedMode;
        FxManager.PlaySound(17, true);
    }

    bool allResolved;

    private void Update()
    {
        if (Input.GetKey(keyLoadGame))
        {
            loadingPanel.gameObject.SetActive(true);
            _currentTimeLoadKeyPressed += Time.deltaTime;
            loadingPanel.progress = _currentTimeLoadKeyPressed / timePressedToLoad;
            return;
        }
        else if (Input.GetKeyUp(keyLoadGame) && _currentTimeLoadKeyPressed >= timePressedToLoad)
        {
            loadingPanel.progress = 0;
            loadingPanel.gameObject.SetActive(false);
            LoadGame();
        }
        else
        {
            loadingPanel.progress = 0;
            loadingPanel.gameObject.SetActive(false);
            _currentTimeLoadKeyPressed = 0;
        }

        RefreshResolved(false);
        RefreshScore();

        if (panelButtonTester.gameObject.activeInHierarchy)
            return;

        if (allResolved)
            return;

        if (resolveMode.modeEnabled)
        {
            RefreshResolved(true);
            return;
        }

        if (AnyHasPressedBigButton(out var controller))
        {
            clueMode.modeEnabled = false;
            resolveMode.RunMode(controller);
            return;
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            clueMode.modeEnabled = false;
            resolveMode.RunMode(0);
            return;
        }

        if (clueMode.modeEnabled)
        {
            RefreshResolved(true);
            return;
        }

        if (XboxBigButtonInput.AnyHasPressed(XboxBigButton.Buttons.A, out controller))
        {
            clueMode.RunMode(controller);
            return;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            clueMode.RunMode(0);
            return;
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            panelButtonTester.gameObject.SetActive(true);
        }

        if (!FxManager.IsPlaying())
            FxManager.PlaySound(17, true);

        header.text = "";
    }

    public static bool ExitButtonsPressed()
    {
        return Input.GetKey(KeyCode.Q)
            && Input.GetKey(KeyCode.W)
            && Input.GetKey(KeyCode.E);
    }

    private void UpdateTurns()
    {
        System.Array.ForEach(GameStatus.instance.score, x => x.waitTurns = Mathf.Max(x.waitTurns-1, 0));
    }

    private void ClueMode_onFinishedMode()
    {
        UpdateTurns();
        FxManager.PlaySound(17, true);
        SaveGame();
    }

    private void ResolveMode_onFinishedMode()
    {
        UpdateTurns();
        FxManager.PlaySound(17, true);
        allResolved = !GameStatus.instance.resolved.Any(x => x == false);
        resultsPanel.gameObject.SetActive(allResolved);
        SaveGame();
    }

    private bool AnyHasPressedBigButton(out Controller controller)
    {
        for(int i=0; i<4; i++)
        {
            if (GameStatus.instance.score[i].waitTurns > 0)
                continue;

            if (XboxBigButtonInput.HasPressed((Controller)i, XboxBigButton.Buttons.BigButton))
            {
                controller = (Controller)i;
                return true;
            }
        }
        controller = default;
        return false;
    }

    private void RefreshResolved(bool blocked)
    {
        for(int i=0; i<animes.Length; i++)
        {
            if (GameStatus.instance.resolved[i])
            {
                int resolvedBy = GameStatus.instance.whoResolved[i];
                animes[i].backgroundColor = blocked || resolvedBy < 0 || resolvedBy > 3
                    ? colorResolved : HUDPlayerScore.GetPlayerColor((GameStatus.PlayerType)resolvedBy);
                animes[i].text = GameStatus.instance.animeList[i];
                if (upperCaseNames)
                    animes[i].text = animes[i].text.ToUpper();
            }
        }
    }

    private void ResetCells()
    {
        for (int i = 0; i < animes.Length; i++)
        {
            animes[i].backgroundColor = colorDefault;
            animes[i].text = "";
        }
    }

    private void RefreshScore()
    {
        foreach(var hud in hudScore)
        {
            var s = GameStatus.instance.score[(int)hud.playerType];
            hud.score = s.score;
            hud.turns = s.waitTurns;
        }
    }

    public void SaveGame()
    {
        StringBuilder result = new StringBuilder();
        var status = GameStatus.instance;
        Dictionary<string, string> stored = new Dictionary<string, string>();
        stored["score_0"] = status.playerGreen.score.ToString();
        stored["turns_0"] = status.playerGreen.waitTurns.ToString();
        stored["score_1"] = status.playerRed.score.ToString();
        stored["turns_1"] = status.playerRed.waitTurns.ToString();
        stored["score_2"] = status.playerBlue.score.ToString();
        stored["turns_2"] = status.playerBlue.waitTurns.ToString();
        stored["score_3"] = status.playerYellow.score.ToString();
        stored["turns_3"] = status.playerYellow.waitTurns.ToString();

        for(int i=0; i<status.resolved.Length; i++)
        {
            stored[$"resolved_{i}"] = status.resolved[i].ToString();
            stored[$"resolvedBy_{i}"] = status.whoResolved[i].ToString();
        }

        foreach(var kv in stored)
        {
            result.AppendLine($"{kv.Key}={kv.Value}");
        }

        File.WriteAllText("gameState.txt", result.ToString());
    }

    public void LoadGame()
    {
        var content = File.ReadAllLines("gameState.txt");
        Dictionary<string, string> stored = new Dictionary<string, string>();
        foreach (var kv in content.Select(l => l.Split('='))) 
        {
            stored[kv[0]] = kv[1];
        }

        var status = GameStatus.instance;
        status.playerGreen.score = int.Parse(stored.GetValueOrDef("score_0", "0"));
        status.playerGreen.waitTurns = int.Parse(stored.GetValueOrDef("turns_0", "0"));
        status.playerRed.score = int.Parse(stored.GetValueOrDef("score_1", "0"));
        status.playerRed.waitTurns = int.Parse(stored.GetValueOrDef("turns_1", "0"));
        status.playerBlue.score = int.Parse(stored.GetValueOrDef("score_2", "0"));
        status.playerBlue.waitTurns = int.Parse(stored.GetValueOrDef("turns_2", "0"));
        status.playerYellow.score = int.Parse(stored.GetValueOrDef("score_3", "0"));
        status.playerYellow.waitTurns = int.Parse(stored.GetValueOrDef("turns_3", "0"));

        for (int i = 0; i < status.resolved.Length; i++)
        {
            status.resolved[i] = bool.Parse(stored.GetValueOrDef($"resolved_{i}", "false"));
            status.whoResolved[i] = int.Parse(stored.GetValueOrDef($"resolvedBy_{i}", "-1"));
        }

        ResetCells();
    }

}
