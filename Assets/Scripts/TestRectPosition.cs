using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRectPosition : MonoBehaviour
{
    public Vector2 position;
    public float width;

    private void Update()
    {
        var transform = GetComponent<RectTransform>();
        transform.anchoredPosition = position;
        transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
    }
}
