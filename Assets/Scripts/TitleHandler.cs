﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleHandler : MonoBehaviour
{
    public float firstDelay;
    public float secondDelay;

    public GameObject[] enableAtFinish;


    private void Start()
    {
        StartCoroutine(MainRoutine());
    }

    private IEnumerator MainRoutine()
    {
        var animator = GetComponent<Animator>();
        FxManager.PlaySound(63);

        yield return new WaitForSeconds(firstDelay);
        animator.SetTrigger("1");

        yield return new WaitForSeconds(secondDelay);
        animator.SetTrigger("2");

        yield return new WaitWhile(() => FxManager.IsPlaying());
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger("3");
        FxManager.PlaySound(2, true);
        yield return null;

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger("4");
        yield return null;

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger("5");
        yield return null;

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger("6");
        yield return null;

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger("7");
        yield return null;

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        yield return null;
        OnFinish();
    }

    private void Update()
    {
        if (GameManager.ExitButtonsPressed())
            OnFinish();
    }

    void OnFinish()
    {
        enableAtFinish.ForEach(g => g.SetActive(true));
        gameObject.SetActive(false);
    }
}
