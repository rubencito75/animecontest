using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentationPanelV4 : MonoBehaviour
{
    public TMPro.TextMeshProUGUI uiServerIP;
    public TMPro.TextMeshProUGUI uiServerState;

    public GameObject[] enableOnFinish;

    private bool ipChecked;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            enableOnFinish.ForEach(x => x.gameObject.SetActive(true));
            gameObject.SetActive(false);
        }

        if (ServerManager.instance == null)
            return;

        if (uiServerState)
        {
            var state = ServerManager.instance.serverStatus;
            uiServerState.text = ServerManager.instance.serverStatus.ToString();
            uiServerState.color = state == ServerManager.ServerStatus.Off ? Color.white : 
                (state == ServerManager.ServerStatus.Listening ? Color.green : Color.red);
        }

        if (uiServerIP && !ipChecked)
        {
            ipChecked = true;
            var ip = ServerManager.instance.GetLocalIPAddress();
            uiServerIP.text = ip;
        }
    }
}
