﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxManager : MonoBehaviour
{
    public AudioClip[] sources;

    private static AudioSource audioSource;
    private static FxManager instance;

    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
        instance = this;
    }

    public static void PlaySound(AudioClip clip, bool loop=false)
    {
        audioSource.clip = clip;
        audioSource.loop = loop;
        audioSource.Play();
    }

    public static void PlaySound(int index, bool loop = false)
    {
        var clip = Array.Find(instance.sources, x => x.name == $"audio{index}");
        PlaySound(clip, loop);
    }

    public static void PlaySound(string clipName, bool loop = false)
    {
        var clip = Array.Find(instance.sources, x => x.name == clipName);
        PlaySound(clip, loop);
    }

    public static void StopMusic()
    {
        audioSource.Stop();
    }

    public static bool IsPlaying() => audioSource.isPlaying;


}
