﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDPlayerScore : MonoBehaviour
{
    [Header("UI Elements")]
    public Image UI_Color;
    public TMPro.TextMeshProUGUI UI_Score;
    public TMPro.TextMeshProUGUI UI_Turns;
    public Image UI_Clock;
    public Outline UI_selectedOutline;

    [Header("Parameters")]
    public GameStatus.PlayerType playerType;
    public int score;
    public int turns;
    public bool selected;

    public static Color GetPlayerColor(GameStatus.PlayerType player)
    {
        switch (player)
        {
            case GameStatus.PlayerType.Green: { return Color.green; }
            case GameStatus.PlayerType.Red: { return Color.red; }
            case GameStatus.PlayerType.Blue: { return Color.blue; }
            case GameStatus.PlayerType.Yellow: { return Color.yellow; }
        }
        return default;
    }

    void UpdateLayout()
    {
        if (UI_Color)
        {
            UI_Color.color = GetPlayerColor(playerType);
        }
        if (UI_Score)
            UI_Score.text = score.ToString();
        if (UI_Turns)
        {
            UI_Turns.text = turns.ToString();
            UI_Turns.gameObject.SetActive(turns > 0);
        }
        if (UI_Clock)
            UI_Clock.gameObject.SetActive(turns > 0);
        if (UI_selectedOutline)
            UI_selectedOutline.effectDistance = selected ? Vector2.one * 8 : Vector2.zero;
    }

    private void Update()
    {
        UpdateLayout();
    }

    private void OnValidate()
    {
        UpdateLayout();
    }
}
