﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresentationPanel : MonoBehaviour
{
    public Transform[] texts;

    public float speed;

    public GameObject[] enableAtFinish;

    IEnumerator Start()
    {
        texts.ForEach(t => t.localScale = Vector3.zero);
        texts.ForEach(t => t.gameObject.SetActive(true));

        float size = 0;

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        yield return null;

        foreach(var text in texts)
        {
            size = 0;
            while (size < 1)
            {
                size += speed * Time.deltaTime;
                size = Mathf.Min(size, 1);
                text.localScale = Vector3.one * size;
                yield return null;
            }
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
            yield return null;
            text.gameObject.SetActive(false);
        }

        enableAtFinish.ForEach(g => g.SetActive(true));
        gameObject.SetActive(false);
    }
}
