﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using XboxBigButton;
using UnityEngine.Video;
using System.IO;
using UnityEngine.Networking;

public class GameAnimeScoreMode : MonoBehaviour
{
    public static float initVolumeVideoPlayer = 1;

    public Image ui_background;
    public TMPro.TextMeshProUGUI ui_animeText;
    public GameAnimeScorePlayer[] players;
    public TMPro.TextMeshProUGUI[] ui_playersScores;
    public TMPro.TextMeshProUGUI ui_realScore;
    public VideoPlayer videoPlayer;

    public string[] triggers;
    public float animationTime = 2.5f;

    private int animeIndex = -1;
    private int mediaIndex = -1;

    private string customAudioFolder = "Audio\\Score";

    private string SpecialScoreToText(int score)
    {
        switch(score)
        {
            case 101: return "ABANDONADO";
            case 102: return "SIN TERMINAR";
            default: return score.ToString();
        }
    }

    public void RunMode(int animeIndex, Action callback)
    {
        ui_background.sprite = GameStatus.instance.animeBackground[animeIndex];
        ui_animeText.text = GameStatus.instance.animeList[animeIndex].ToUpper();
        gameObject.SetActive(true);
        //videoPlayer.url = "AnimeData\\Death Note\\02.mp4"; //GameStatus.instance.animeExtraData[animeIndex].media.ElementAt(0);
        //videoPlayer.Play();
        this.animeIndex = animeIndex;
        this.mediaIndex = -1;
        StartCoroutine(JokeRoutine(animeIndex));
        StartCoroutine(MainRoutine(animeIndex, callback));
    }

    float timeCheckStopped = 1f;

    private void Update()
    {
        var media = GameStatus.instance.animeExtraData[animeIndex].media;
        System.IO.File.WriteAllLines("tmpAnimeMediaContent.txt", media);
        
        if (media.Length == 0)
            return;

        timeCheckStopped -= Time.deltaTime;

        bool videoChanged = false;
        if ((timeCheckStopped <= 0 && !videoPlayer.isPlaying) ||
            XboxBigButtonInput.AnyHasPressedDown(Buttons.Right, out var c1))
        {
            mediaIndex = Mathf.RoundToInt(Mathf.Repeat(mediaIndex + 1, media.Length));
            videoChanged = true;
        }

        if (XboxBigButtonInput.AnyHasPressedDown(Buttons.Left, out var c2))
        {
            mediaIndex = Mathf.RoundToInt(Mathf.Repeat(mediaIndex - 1, media.Length));
            videoChanged = true;
        }

        if (videoChanged)
        {
            videoPlayer.Stop();
            videoPlayer.url = media[mediaIndex];
            videoPlayer.Play();
            timeCheckStopped = 1f;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            initVolumeVideoPlayer = Math.Min(initVolumeVideoPlayer + 0.05f, 1);
            videoPlayer.SetDirectAudioVolume(0, initVolumeVideoPlayer);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            initVolumeVideoPlayer = Math.Max(initVolumeVideoPlayer - 0.05f, 0);
            videoPlayer.SetDirectAudioVolume(0, initVolumeVideoPlayer);
        }
    }

    bool isApplicationQuitting = false;
    private void OnApplicationQuit()
    {
        isApplicationQuitting = true;
    }

    private void OnDisable()
    {
        if (isApplicationQuitting) return;

        videoPlayer.Stop();
    }

    IEnumerator JokeRoutine(int animeIndex)
    {
        var extraData = GameStatus.instance.animeExtraData[animeIndex];
        if (extraData.jokeTime <= 0)
            yield break;

        if (extraData.jokeName != null)
            ui_animeText.text = extraData.jokeName;

        if (extraData.jokeBackground != null)
            ui_background.sprite = extraData.jokeBackground;

        yield return new WaitForSeconds(extraData.jokeTime);

        ui_background.sprite = GameStatus.instance.animeBackground[animeIndex];
        ui_animeText.text = GameStatus.instance.animeList[animeIndex].ToUpper();
    }

    IEnumerator MainRoutine(int animeIndex, Action callback)
    {
        MusicManager.StopMusic();
        videoPlayer.SetDirectAudioVolume(0, initVolumeVideoPlayer);
        //MusicManager.PlayMusic(animeIndex);

        players.ForEach(p => p.enabled = false);
        var animator = GetComponent<Animator>();
        yield return new WaitForSeconds(1);
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));

        animator.SetTrigger(triggers[0]);

        yield return new WaitForSeconds(1.5f);
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));

        animator.SetTrigger(triggers[1]);
        yield return new WaitForSeconds(animationTime);
        players.ForEach(p => p.score = 0);
        players.ForEach(p => p.enabled = true);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        var pScores = players.Select(p => p.score).ToArray();
        players.ForEach(p => p.enabled = false);
        animator.SetTrigger(triggers[2]);
        yield return new WaitForSeconds(animationTime);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        for (int i = 0; i < pScores.Length; i++) ui_playersScores[i].text = pScores[i].ToString();
        //animator.SetTrigger(triggers[6]);
        //yield return new WaitForSeconds(0.1f);
        animator.SetTrigger(triggers[3]);
        FxManager.PlaySound(9);
        yield return new WaitForSeconds(animationTime);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger(triggers[4]);
        var realScore = GameStatus.instance.animeScore[animeIndex];
        ui_realScore.text = realScore <= 10 ? realScore.ToString() : SpecialScoreToText(realScore);
        FxManager.PlaySound(99);
        if (Directory.Exists(customAudioFolder))
        {
            var dir = new DirectoryInfo(customAudioFolder);
            var files = dir.GetFiles().Where(x => Path.GetFileNameWithoutExtension(x.Name).Split('_')[0] == realScore.ToString()).ToArray();
            if (files.Length > 0)
            {
                yield return new WaitForSeconds(1);
                var file = string.Format("file://{0}", files[UnityEngine.Random.Range(0, files.Length)]);
                using (var www = UnityWebRequestMultimedia.GetAudioClip(file, AudioType.MPEG))
                {
                    yield return www.SendWebRequest();

                    var vol = videoPlayer.GetDirectAudioVolume(0);
                    videoPlayer.SetDirectAudioVolume(0, vol / 4);
                    var clip = DownloadHandlerAudioClip.GetContent(www);
                    FxManager.PlaySound(clip);
                    yield return new WaitForSeconds(clip.length);
                    videoPlayer.SetDirectAudioVolume(0, vol);
                }
            }
        }
        yield return new WaitForSeconds(animationTime);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        animator.SetTrigger(triggers[5]);
        yield return new WaitForSeconds(animationTime);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        var minScoreDiff = pScores.Aggregate(int.MaxValue, (acc, val) => Mathf.Min(acc, Mathf.Abs(realScore - val)));

        for(int i=0; i<pScores.Length; i++)
        {
            if (realScore > 10) continue;
            if (Mathf.Abs(realScore - pScores[i]) == minScoreDiff)
            {
                GameManager.instance.scoreWinPanel.ShowPanel((Controller)i, GameStatus.instance.scoreOnWinAnimeScore, 0, null);
                yield return new WaitWhile(() => GameManager.instance.scoreWinPanel.gameObject.activeInHierarchy);
                GameStatus.instance.score[i].score += GameStatus.instance.scoreOnWinAnimeScore;
            }
        }

        MusicManager.StopMusic();
        callback?.Invoke();
        gameObject.SetActive(false);
    }
}
