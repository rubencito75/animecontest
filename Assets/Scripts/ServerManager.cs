using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibHttpServer;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Sockets;

public class ServerManager : MonoBehaviour
{
    public enum ServerStatus { Off, Listening, Failed }

    [System.Serializable]
    private class ClueState
    {
        public bool enabled;
        public int index;
    }

    [System.Serializable]
    private class ResolveState
    {
        public bool enabled;
        public int index;
    }

    [System.Serializable]
    private class State
    {
        public ClueState clueMode = new ClueState { enabled = false, index = 0 };
        public ResolveState resolveMode = new ResolveState { enabled = false, index = 0 };
    }

    [System.Serializable]
    private class AnimeData
    {
        public string name;
        public int malID;
        public string review;
        public int score;
    }

    [System.Serializable]
    private class GameData
    {
        public AnimeData[] animeDataList;
        public string[] malUsersList;
    }

    public int port = 18081;

    private HttpServer server;
    private List<HttpListenerResponse> stateListeners;
    private State state;
    public static ServerManager instance;

    public ServerStatus serverStatus { get; private set; }
    public string lastError { get; private set; }

    private void OnEnable()
    {
        instance = this;
        serverStatus = ServerStatus.Off;
        try
        {
            server = new HttpServer();
            server.Listen("*", port);
        }
        catch(System.Exception e)
        {
            serverStatus = ServerStatus.Failed;
            lastError = e.Message;
            return;
        }
        serverStatus = ServerStatus.Listening;
        stateListeners = new List<HttpListenerResponse>();
        state = new State();
        Listeners();
        //StartCoroutine(StatusRoutine());
    }

    private void OnDisable()
    {
        if (server != null)
            server.Close();

        StopAllCoroutines();
    }

    private void Listeners()
    {
        server.AddEndpoint("POST", "/api/cluemode/setbutton", (req, res) =>
        {
            var body = req.GetBody();
            var json = JObject.Parse(body);
            foreach(var item in json)
            {
                var key = item.Key;
                var value = (bool)item.Value;

                // no, no_q, q, yes_q, yes
                GameManager.instance.clueMode.remoteController.SetButton(key, value);
            }

            res.Close();
        });

        server.AddEndpoint("GET", "/api/onchangestate", (req, res) =>
        {
            stateListeners.Add(res);
        });

        server.AddEndpoint("GET", "/api/getstate", (req, res) =>
        {
            var body = JsonUtility.ToJson(state);
            res.StatusCode = 200;
            res.WriteBody(body);
            res.Close();
        });

        server.AddEndpoint("GET", "/api/getgamedata", (req, res) =>
        {
            var status = GameStatus.instance;
            var gamedata = new GameData();
            var animes = new List<AnimeData>();

            for(int i=0; i<status.animeList.Length; i++)
            {
                animes.Add(new AnimeData()
                {
                    name = status.animeList[i],
                    score = status.animeScore[i],
                    malID = status.animeExtraData[i].malID,
                    review = status.animeExtraData[i].review
                });
            }
            gamedata.animeDataList = animes.ToArray();
            gamedata.malUsersList = status.malUsers;

            var body = JsonUtility.ToJson(gamedata);
            res.StatusCode = 200;
            res.WriteBody(body);
            res.Close();
        });
    }

    void Update()
    {
        bool changed = false;

        var clue = GameManager.instance?.clueMode;
        if (clue != null)
        {
            changed |= state.clueMode.enabled != clue.statusModeEnabled;
            changed |= state.clueMode.index != clue.statusModeIndex;

            state.clueMode.enabled = clue.statusModeEnabled;
            state.clueMode.index = clue.statusModeIndex;
        }

        var resolve = GameManager.instance?.resolveMode;
        if (resolve != null)
        {
            changed |= state.resolveMode.enabled != resolve.modeEnabled;
            changed |= state.resolveMode.index != resolve.selectedIndex;

            state.resolveMode.enabled = resolve.modeEnabled;
            state.resolveMode.index = resolve.selectedIndex;
        }

        if (changed)
        {
            foreach (var res in stateListeners)
            {
                try
                {
                    var body = JsonUtility.ToJson(state);
                    res.WriteBody(body);
                    res.Close();
                }
                catch { }
            }
            stateListeners.Clear();
        }
    }

    public string GetLocalIPAddress()
    {
        string localIP = null;
        using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
        {
            try
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }
            catch (SocketException) { }
        }
        return localIP;
    }
}
