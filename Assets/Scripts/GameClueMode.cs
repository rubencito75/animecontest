﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using XboxBigButton;

public class GameClueMode : MonoBehaviour
{
    public string textHeader;
    public string textHeaderContinue = "...";
    public Color colorYes;
    public Color colorYesQuote;
    public Color colorNo;
    public Color colorNoQuote;
    public Color colorUnknown;
    public Color colorSelected;
    public Color colorDefault;

    public RemoteController remoteController;

    private AnimeOption[] animes;
    private TMPro.TextMeshProUGUI header;

    internal event System.Action onFinishedMode;

    public void Start()
    {
        animes = GameManager.instance.animes;
        header = GameManager.instance.header;
        remoteController = new RemoteController();
    }

    public bool modeEnabled;
    private int index;
    private bool playedFxComplete;

    public bool statusModeEnabled => modeEnabled;
    public int statusModeIndex => index;
   

    public void ResetMode()
    {
        modeEnabled = false;
        index = 0;
        playedFxComplete = false;
        for(int i=0; i<animes.Length; i++)
        {
            if (GameStatus.instance.resolved[i])
                continue;

            animes[i].backgroundColor = colorDefault;
            animes[i].text = "";
        }
        foreach (var hud in GameManager.instance.hudScore)
            hud.selected = false;
    }

    public void RunMode(Controller c)
    {
        ResetMode();
        modeEnabled = true;
        GameManager.instance.hudScore.First(x => x.playerType == (GameStatus.PlayerType)c).selected = true;
        FxManager.PlaySound(10, true);
        remoteController?.Reset();
    }

    private void Update()
    {
        if (!modeEnabled)
            return;

        if (GameManager.ExitButtonsPressed())
        {
            ResetMode();
            onFinishedMode?.Invoke();
        }

        if (!FxManager.IsPlaying())
        {
            FxManager.PlaySound(10, true);
        }

        if (index >= animes.Length)
        {
            header.text = textHeaderContinue;
            if (!playedFxComplete)
            {
                FxManager.PlaySound(99);
                playedFxComplete = true;
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                ResetMode();
                onFinishedMode?.Invoke();
            }
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                do
                    index--;// = Mathf.Max(index - 1, minIndex, 0);
                while (GameStatus.instance.resolved[index]);
                playedFxComplete = false;
            }

            return;
        }

        header.text = textHeader;

        if (GameStatus.instance.resolved[index])
        {
            index++;
            return;
        }

        var anime = animes[index];
        anime.backgroundColor = colorSelected;
        anime.text = "";

        if (Input.GetKeyDown(KeyCode.Y) || remoteController.GetButton("yes"))
        {
            anime.text = "SI";
            anime.backgroundColor = colorYes;
            index++;
        }
        if (Input.GetKeyDown(KeyCode.U) || remoteController.GetButton("yes_q"))
        {
            anime.text = "SI?";
            anime.backgroundColor = colorYesQuote;
            index++;
        }
        if (Input.GetKeyDown(KeyCode.N) || remoteController.GetButton("no"))
        {
            anime.text = "NO";
            anime.backgroundColor = colorNo;
            index++;
        }
        if (Input.GetKeyDown(KeyCode.M) || remoteController.GetButton("no_q"))
        {
            anime.text = "NO?";
            anime.backgroundColor = colorNoQuote;
            index++;
        }
        if (Input.GetKeyDown(KeyCode.X) || remoteController.GetButton("q"))
        {
            anime.text = "?";
            anime.backgroundColor = colorUnknown;
            index++;
        }
        int minIndex = System.Array.IndexOf(GameStatus.instance.resolved, false);
        if (Input.GetKeyDown(KeyCode.Backspace) || remoteController.GetButton("undo"))
        {
            anime.backgroundColor = colorDefault;
            do
                index = Mathf.Max(index - 1, minIndex);
            while (GameStatus.instance.resolved[index]);
        }
    }
}
