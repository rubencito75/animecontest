using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class GameConfigLoader : MonoBehaviour
{
    public string fileConfig = "gameConfig.txt";
    public string animeFolder = "AnimeData";
    public string fileMalUsers = "malUsers.txt";
    public GameStatus gameStatus;
    public GameManager gameManager;
    public AnimeGridGenerator animeGenerator;

    private void Start()
    {
        LoadFile();
    }

    void LoadFile()
    {
        string[] content = File.ReadAllLines(fileConfig);
        var dicc = content.Select(x => x.Split('=').Select(x => x.Trim())).ToDictionary(x => x.ElementAt(0), x => x.ElementAt(1));
        gameStatus.scoreOnWin = int.Parse(dicc.GetValueOrDef("scoreOnWin", "0"));
        gameStatus.scoreOnLose = int.Parse(dicc.GetValueOrDef("scoreOnLose", "0"));
        gameStatus.scoreOnWinAnimeScore = int.Parse(dicc.GetValueOrDef("winInAnimeScore", "0"));
        gameStatus.turnsOnWin = int.Parse(dicc.GetValueOrDef("turnsOnWin", "0"));
        gameStatus.turnsOnLose = int.Parse(dicc.GetValueOrDef("turnsOnLose", "0"));
        GameAnimeScoreMode.initVolumeVideoPlayer = float.Parse(dicc.GetValueOrDef("videoPlayerVolume", "1"));

        var animeListFile = dicc["animeListFile"];
        var animeContent = File.ReadAllLines(animeListFile);

        var resolution = animeContent[0].Split(';');

        var animeList = new List<string>();
        //var animeBackground = new List<string>();
        var animeBackground = new List<Sprite>();
        var animeScore = new List<int>();
        var animeExtraData = new List<GameStatus.AnimeExtraData>();

        for(int i=1; i<animeContent.Length; i++)
        {
            var line = animeContent[i].Split(';');
            var folder = Path.GetFullPath($"{animeFolder}\\{line[0]}");
            animeList.Add(line[0]);
            animeScore.Add(int.Parse(line[1]));
            //animeBackground.Add($"{animeFolder}\\background.jpg");

            var tex = new Texture2D(2, 2);
            var imgBytes = File.ReadAllBytes($"{folder}\\background.jpg");
            tex.LoadImage(imgBytes);
            var sprite = Sprite.Create(tex, Rect.MinMaxRect(0, 0, tex.width, tex.height), Vector2.one * 0.5f);
            animeBackground.Add(sprite);


            var extraData = new GameStatus.AnimeExtraData();
            animeExtraData.Add(extraData);
            extraData.media = Directory.GetFiles(folder, "*.mp4");

            if (File.Exists($"{folder}\\info.txt"))
            {
                var info = File.ReadAllLines($"{folder}\\info.txt").Select(x => (x.IndexOf("#") >= 0 ? x.Substring(0, x.IndexOf("#")) : x).Trim()).Where(x => x.Length > 0).ToArray();
                var infoDicc = info.ToDictionary(x => x.Split('=').ElementAt(0), x => x.Split('=').ElementAt(1));

                if (infoDicc.TryGetValue("name", out string name))
                    animeList[animeList.Count - 1] = name;

                if (infoDicc.TryGetValue("jokeName", out string jokeName)) extraData.jokeName = jokeName;
                if (infoDicc.TryGetValue("jokeTime", out string jokeTime)) extraData.jokeTime = float.Parse(jokeTime);
                if (infoDicc.TryGetValue("jokeBackground", out string jokeBackground))
                {
                    var joketex = new Texture2D(2, 2);
                    var jokeimg = File.ReadAllBytes(Path.Combine(folder, jokeBackground));
                    joketex.LoadImage(jokeimg);
                    extraData.jokeBackground = Sprite.Create(joketex, Rect.MinMaxRect(0, 0, joketex.width, joketex.height), Vector2.one * 0.5f);
                }
                if (infoDicc.TryGetValue("malID", out string malID)) extraData.malID = int.Parse(malID);
                if (infoDicc.TryGetValue("review", out string review)) extraData.review = review.Replace("\\n", "\n").Replace("\\t", "\t");
            }
        }

        if (File.Exists(fileMalUsers))
        {
            gameStatus.malUsers = File.ReadAllLines(fileMalUsers).Select(x => x.Trim()).Where(x => x.Length > 0).ToArray();
        }

        gameStatus.animeList = animeList.ToArray();
        gameStatus.animeBackground = animeBackground.ToArray();
        gameStatus.animeScore = animeScore.ToArray();
        gameStatus.animeExtraData = animeExtraData.ToArray();
        gameStatus.resolved = new bool[animeList.Count];

        animeGenerator.rows = int.Parse(resolution[0]);
        animeGenerator.columns = int.Parse(resolution[1]);
        animeGenerator.amount = animeList.Count;
        gameStatus.animeRows = animeGenerator.rows;
        gameStatus.animeColumns = animeGenerator.columns;

        animeGenerator.Generate();
        animeGenerator.gameObject.SetActive(true);
        gameStatus.gameObject.SetActive(true);
        //gameManager.gameObject.SetActive(true);
    }

}
