﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class GameResolveMode : MonoBehaviour
{
    public string textHeader;
    public string textHeaderContinue = "...";
    public Color colorYes;
    public Color colorNo;
    public Color colorUnknown;
    public Color colorSelected;
    public Color colorDefault;

    public int sideSumIndex = 10;
    public float timePressedYes = 3;

    private AnimeOption[] animes;
    private TMPro.TextMeshProUGUI header;
    public float _currentTimePressed;

    internal event System.Action onFinishedMode;

    public void Start()
    {
        animes = GameManager.instance.animes;
        header = GameManager.instance.header;

        if (GameStatus.instance.animeRows > 0)
            sideSumIndex = GameStatus.instance.animeRows;
    }

    public bool modeEnabled;
    private bool flagUpdate;
    private bool paused;
    private bool pressed;
    private int resolvedStatus; 
    private int index;
    private XboxBigButton.Controller controller;

    public int selectedIndex => index;

    public void ResetMode()
    {
        modeEnabled = false;
        paused = false;
        flagUpdate = false;
        pressed = false;
        resolvedStatus = 0;
        index = 0;
        foreach (var hud in GameManager.instance.hudScore)
            hud.selected = false;
    }

    public void RunMode(XboxBigButton.Controller controller)
    {
        ResetMode();
        this.controller = controller;
        modeEnabled = true;
        GameManager.instance.hudScore.First(x => x.playerType == (GameStatus.PlayerType)controller).selected = true;
        FxManager.PlaySound(6);
        index = Array.IndexOf(GameStatus.instance.resolved, false);
    }

    private void ResetColors()
    {
        for(int i=0; i<animes.Length; i++)
        {
            if (!GameStatus.instance.resolved[i])
            {
                animes[i].backgroundColor = colorDefault;
                animes[i].text = "";
            }
        }
    }

    private void Update()
    {
        if (paused)
            return;

        if (!modeEnabled)
            return;

        if (!flagUpdate)
        {
            flagUpdate = true;
            return;
        }

        if (GameManager.ExitButtonsPressed())
        {
            ResetMode();
            ResetColors();
            onFinishedMode?.Invoke();
        }

        ResetColors();
        index = Mathf.Clamp(index, 0, animes.Length);
        var anime = animes[index];

        anime.backgroundColor = !pressed ? colorSelected
            : resolvedStatus == 0 ? colorUnknown
            : resolvedStatus == 1 ? colorYes
            : colorNo;

        if (pressed && !FxManager.IsPlaying() && resolvedStatus == 0)
        {
            FxManager.PlaySound(3, true);
        }

        if (pressed && resolvedStatus > 0)
        {
            GameStatus.instance.resolved[index] = resolvedStatus == 1;
            if (resolvedStatus == 2) anime.text = "NOOOOOOO";
            else if (resolvedStatus == 1 
                && GameStatus.instance.animeExtraData[index].jokeName != null 
                && GameStatus.instance.animeExtraData[index].nameBackup == null)
            {
                GameStatus.instance.animeExtraData[index].nameBackup = GameStatus.instance.animeList[index];
                GameStatus.instance.animeList[index] = GameStatus.instance.animeExtraData[index].jokeName;
            }

            header.text = textHeaderContinue;
            if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
            {
                if (GameStatus.instance.animeExtraData[index].nameBackup != null) 
                    GameStatus.instance.animeList[index] = GameStatus.instance.animeExtraData[index].nameBackup;
                GameStatus.instance.resolved[index] = false;
                ResetMode();
                ResetColors();
                onFinishedMode?.Invoke();
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                int scoreAdd = resolvedStatus == 1 ? GameStatus.instance.scoreOnWin : GameStatus.instance.scoreOnLose;
                int turnsAdd = resolvedStatus == 1 ? GameStatus.instance.turnsOnWin : GameStatus.instance.turnsOnLose;
                paused = true;

                GameManager.instance.scoreWinPanel.ShowPanel(controller, scoreAdd, turnsAdd, () =>
                {
                    GameStatus.instance.score[(int)controller].score += scoreAdd;
                    GameStatus.instance.score[(int)controller].waitTurns += turnsAdd+1;

                    if (resolvedStatus == 1 && GameStatus.instance.animeScore[index] >= 0)
                    {
                        if (GameStatus.instance.animeExtraData[index].nameBackup != null)
                            GameStatus.instance.animeList[index] = GameStatus.instance.animeExtraData[index].nameBackup;

                        GameStatus.instance.whoResolved[index] = (int)controller;
                        GameManager.instance.animeScoreMode.RunMode(index, () =>
                        {
                            ResetMode();
                            ResetColors();
                            onFinishedMode?.Invoke();
                        });
                    }
                    else
                    {
                        ResetMode();
                        ResetColors();
                        onFinishedMode?.Invoke();
                    }
                });
            }
            return;
        }
        header.text = textHeader;

        if (!pressed)
        {
            ReadXArrows();
        }
        else if (resolvedStatus == 0)
        {
            if (XboxBigButtonInput.HasPressedDown(controller, XboxBigButton.Buttons.B))
            {
                pressed = false;
            }
            if (Input.GetKeyUp(KeyCode.Y) && _currentTimePressed >= timePressedYes)
            {
                resolvedStatus = 1;
                FxManager.PlaySound(19, false);                    
            }
            _currentTimePressed = Input.GetKey(KeyCode.Y) && !Input.GetKey(KeyCode.N) ? _currentTimePressed + Time.deltaTime : 0;
            if (Input.GetKeyDown(KeyCode.N))
            {
                resolvedStatus = 2;
                FxManager.PlaySound(20, false);
            }
        }
    }

    private void ReadXArrows()
    {
        var resolved = GameStatus.instance.resolved;
        if (XboxBigButtonInput.HasPressedDown(controller, XboxBigButton.Buttons.Down))
        {
            do
                index = (int)Mathf.Repeat(index + 1, animes.Length);
            while (resolved[index]);
        }
        if (XboxBigButtonInput.HasPressedDown(controller, XboxBigButton.Buttons.Up))
        {
            do
                index = (int)Mathf.Repeat(index - 1, animes.Length);
            while (resolved[index]);
        }
        if (XboxBigButtonInput.HasPressedDown(controller, XboxBigButton.Buttons.Left))
        {
            if (index - sideSumIndex >= 0 && !resolved[index - sideSumIndex])
                index -= sideSumIndex;
        }
        if (XboxBigButtonInput.HasPressedDown(controller, XboxBigButton.Buttons.Right))
        {
            if (index + sideSumIndex < animes.Length && !resolved[index + sideSumIndex])
                index += sideSumIndex;
        }
        if (XboxBigButtonInput.HasPressedDown(controller, XboxBigButton.Buttons.BigButton))
        {
            pressed = true;
            FxManager.PlaySound(9);
        }
    }
}
