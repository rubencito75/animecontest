﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XboxBigButton;

public class GameAnimeScorePlayer : MonoBehaviour
{
    public GameStatus.PlayerType player;
    public Image ui_upArrow;
    public Image ui_downArrow;

    public Color colorOff;
    public Color colorOn;

    public int score;

    private void Update()
    {
        var c = (Controller)player;
        ui_upArrow.color = score < 10 && XboxBigButtonInput.HasPressed(c, Buttons.A) ? colorOn : colorOff;
        ui_downArrow.color = score > 0 && XboxBigButtonInput.HasPressed(c, Buttons.B) ? colorOn : colorOff;

        if (score < 10 && XboxBigButtonInput.HasPressedDown(c, Buttons.A))
        {
            score++;
        }
        if (score > 0 && XboxBigButtonInput.HasPressedDown(c, Buttons.B))
        {
            score--;
        }
    }
}
