﻿using System;
using UnityEngine;
using XboxBigButton;
using System.Linq;
using System.Collections.Generic;

public class XboxBigButtonJoystick : MonoBehaviour
{
    private XboxBigButtonDevice _device;
    private Buttons[] _state = new Buttons[4];

    private static XboxBigButtonJoystick _instance;

    public static bool GetButton(Controller playerColor, Buttons button)
    {
        bool success = (_instance._state[(int)playerColor] & button) == button;
        _instance._state[(int)playerColor] &= ~button; // Consume the button state
        return success;
    }

    public static bool GetPlayer(Controller playerColor)
    {
        return _instance._state[(int)playerColor] > 0;
    }

    private float _hor = 0f;
    private float _easeStrength = 0.5f;

    public static float GetAxis(Controller playerColor, string axisName)
    {
        int amount = 0;

        if (axisName == "Horizontal")
        {
            if ((_instance._state[(int)playerColor] & Buttons.Right) == Buttons.Right)
            {
                amount += 1;
                _instance._state[(int)playerColor] &= ~Buttons.Right;
            }

            if ((_instance._state[(int)playerColor] & Buttons.Left) == Buttons.Left)
            {
                amount += -1;
                _instance._state[(int)playerColor] &= ~Buttons.Left;
            }
        }
        else if (axisName == "Vertical")
        {
            if ((_instance._state[(int)playerColor] & Buttons.Up) == Buttons.Up)
            {
                amount += 1;
                _instance._state[(int)playerColor] &= ~Buttons.Up;
            }

            if ((_instance._state[(int)playerColor] & Buttons.Down) == Buttons.Down)
            {
                amount += -1;
                _instance._state[(int)playerColor] &= ~Buttons.Down;
            }
        }

        if (amount == 0)
            return 0.0f;

        // Attempt to do some easing of the movement, yeah this doesn't really work that well
        float val = 1.0f * amount;
        _instance._hor = Mathf.Lerp(_instance._hor, val, _instance._easeStrength);
        return _instance._hor;
    }

    // Use this for initialization
    void Start()
    {
        // Must have a dummy line to instantiate Loom correctly (sets up the dummy game object that we will invoke everuthing on)
        var tmp = Loom.Current.GetComponent<Loom>();

        // Store the singleton
        _instance = this;

        if (_device == null)
        {
            _state = new Buttons[4];
            _device = new XboxBigButtonDevice();
            _device.ButtonStateChanged += _device_ButtonStateChanged;
            _device.Connect();
        }
    }

    private void CleanUpJoystickDevice()
    {
        if (_device != null)
        {
            _device.ButtonStateChanged -= _device_ButtonStateChanged;
            _device.Disconnect();
            //_device.Dispose();
            _device = null;
            _state = null;
        }
    }

    void OnApplicationQuit()
    {
        CleanUpJoystickDevice();
    }

    void OnDestroy()
    {
        CleanUpJoystickDevice();
    }

    private void _device_ButtonStateChanged(object sender, XboxBigButtonDeviceEventArgs e)
    {
        var controller = e.Controller;
        var buttonState = e.ButtonState;

        Loom.QueueOnMainThread(() =>
        {
            UpdateButtonStateDictionary(controller, buttonState);
        });
    }

    private void UpdateButtonStateDictionary(Controller c, Buttons b)
    {
        _instance._state[(int)c] |= b;
    }
}

public static class XboxBigButtonInput
{
    private const float releaseDelay = 0.2f;
    private class ButtonState
    {
        public float prevTime = float.MinValue;
        public float currentTime = float.MinValue;

        public bool IsPressed => Time.time - currentTime < releaseDelay;
        public bool IsPressedDown => Time.time == currentTime && currentTime - prevTime > releaseDelay;
    }

    static Dictionary<Buttons, ButtonState>[] stateDicc;

    static XboxBigButtonInput()
    {
        stateDicc = new Dictionary<Buttons, ButtonState>[4];
        foreach (Controller c in Enum.GetValues(typeof(Controller)))
        {
            stateDicc[(int)c] = new Dictionary<Buttons, ButtonState>();
            foreach (Buttons b in Enum.GetValues(typeof(Buttons)))
            {
                stateDicc[(int)c][b] = new ButtonState();
            }
        }
    }

    //static Buttons[] prevState = new Buttons[4];
    //static Buttons[] currentState = new Buttons[4];
    static float currentTime;

    static void RefreshState()
    {
        if (currentTime == Time.time)
            return;

        currentTime = Time.time;
        
        foreach(Controller c in Enum.GetValues(typeof(Controller)))
        {
            foreach(Buttons b in Enum.GetValues(typeof(Buttons)))
            {
                if (b == Buttons.None) continue;

                if (XboxBigButtonJoystick.GetButton(c, b))
                {
                    var state = stateDicc[(int)c][b];
                    state.prevTime = state.currentTime;
                    state.currentTime = Time.time;
                }

                //if (XboxBigButtonJoystick.GetButton(c, b))
                //    currentState[(int)c] |= b;
            }
        }
    }

    public static bool HasPressed(Controller c, Buttons b)
    {
        RefreshState();
        return stateDicc[(int)c][b].IsPressed;
    }

    public static bool HasPressedDown(Controller c, Buttons b)
    {
        RefreshState();
        return stateDicc[(int)c][b].IsPressedDown;
    }

    public static bool AnyHasPressed(Buttons b, out Controller c)
    {
        RefreshState();
        var index = Array.FindIndex(stateDicc, dic => dic[b].IsPressed);
        c = index >= 0 ? (Controller)index : default;
        return index >= 0;
    }

    public static bool AnyHasPressedDown(Buttons b, out Controller c)
    {
        RefreshState();
        var index = Array.FindIndex(stateDicc, dic => dic[b].IsPressedDown);
        c = index >= 0 ? (Controller)index : default;
        return index >= 0;
    }
}