﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private static AudioSource audioSource;

    
    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlayMusic(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
    }

    public static void PlayMusic(int index)
    {
        PlayMusic(GameStatus.instance.animeMusic[index]);
    }

    public static void StopMusic()
    {
        audioSource.Stop();
    }

    public static bool IsPlaying() => audioSource.isPlaying;

    public static float Volume
    {
        get => audioSource.volume;
        set
        {
            audioSource.volume = value;
        }
    }
}
