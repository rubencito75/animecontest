﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

static class ArrayExtensions
{
    public static void ForEach<T>(this T[] array, Action<T> predicate)
    {
        Array.ForEach(array, predicate);
    }

    public static T2 GetValueOrDef<T1, T2>(this Dictionary<T1, T2> dicc, T1 key, T2 defValue)
    {
        if (!dicc.TryGetValue(key, out T2 val))
            return defValue;

        return val;
    }
}
