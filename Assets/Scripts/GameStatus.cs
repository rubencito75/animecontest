﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour
{
    public enum PlayerType { Green, Red, Blue, Yellow };

    [System.Serializable]
    public class Player
    {
        public int score;
        public int waitTurns;
    }

    [System.Serializable]
    public class AnimeExtraData
    {
        public string[] media;
        public string jokeName;
        public Sprite jokeBackground;
        public float jokeTime;
        public string nameBackup; // Store orig name
        public int malID = -1;
        public string review;
    }

    public string[] animeList;
    public string[] animeBackgroundNew;
    public Sprite[] animeBackground;
    public AudioClip[] animeMusic;
    public int[] animeScore;
    public AnimeExtraData[] animeExtraData;
    public string[] malUsers;

    public int animeRows = -1;
    public int animeColumns = -1;

    public bool[] resolved;
    public int[] whoResolved;

    public int scoreOnWin;
    public int scoreOnLose;
    public int turnsOnWin;
    public int turnsOnLose;
    public int scoreOnWinAnimeScore;

    public Player playerGreen;
    public Player playerRed;
    public Player playerBlue;
    public Player playerYellow;
    internal Player[] score { get; private set; }

    public static GameStatus instance;

    private void OnEnable()
    {
        instance = this;
        score = new Player[] { playerGreen, playerRed, playerBlue, playerYellow };
        //for (int i = 0; i < resolved.Length; i++) resolved[i] = true;
    }

    private void Start()
    {
        whoResolved = new int[resolved.Length];
        for (int i = 0; i < whoResolved.Length; i++) whoResolved[i] = -1;
    }

    private void Update()
    {
        XboxBigButtonInput.AnyHasPressed(XboxBigButton.Buttons.BigButton, out var c);
    }

}
