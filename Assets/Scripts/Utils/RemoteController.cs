﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class RemoteController
{
    private Dictionary<string, bool> buttons = new Dictionary<string, bool>();

    public bool GetButton(string button)
    {
        if (buttons.ContainsKey(button))
        {
            bool val = buttons[button];
            buttons[button] = false;
            return val;
        }

        return false;
    }

    public void SetButton(string button, bool value)
    {
        buttons[button] = value;
    }

    public void Reset()
    {
        buttons.Keys.ToArray().ForEach(k => buttons[k] = false);
    }
}
