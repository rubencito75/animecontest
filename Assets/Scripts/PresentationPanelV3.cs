using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresentationPanelV3 : MonoBehaviour
{
    [System.Serializable]
    public class PhraseItem
    {
        public string str;
        public float time;
        public AudioClip audioExtra;
        public float audioTimeDelay;
    }

    public float initDelay;
    public AudioClip audioHit;
    public AudioClip audioNumber;
    public AudioClip audioSubtitle;
    public PhraseItem[] phrase1;
    public PhraseItem[] phrase2;
    public PhraseItem[] phrase3;
    public PhraseItem[] phrase4;

    public TMPro.TextMeshProUGUI ui_Phrase1;
    public TMPro.TextMeshProUGUI ui_Phrase2;
    public TMPro.TextMeshProUGUI ui_Phrase3;
    public TMPro.TextMeshProUGUI ui_Phrase4;

    public GameObject[] enableAtFinish;

    IEnumerator Start()
    {
        ui_Phrase1.text = "";
        ui_Phrase2.text = "";
        ui_Phrase3.text = "";
        ui_Phrase4.text = "";

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        yield return new WaitForSeconds(initDelay);

        yield return PrintPhrase(phrase1, ui_Phrase1, audioHit);
        yield return PrintPhrase(phrase2, ui_Phrase2, audioNumber);
        AudioSource.PlayClipAtPoint(audioSubtitle, Camera.main.transform.position);
        yield return PrintPhrase(phrase3, ui_Phrase3);
        yield return PrintPhrase(phrase4, ui_Phrase4);

        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return));
        yield return null;
        enableAtFinish.ForEach(g => g.SetActive(true));
        gameObject.SetActive(false);
    }

    IEnumerator PrintPhrase(PhraseItem[] phrase, TMPro.TextMeshProUGUI uiElement, AudioClip audioElement=null)
    {
        foreach (var item in phrase)
        {
            uiElement.text += item.str;
            if (audioElement != null)
                AudioSource.PlayClipAtPoint(audioElement, Camera.main.transform.position);
            yield return new WaitForSeconds(item.audioTimeDelay);
            if (item.audioExtra != null)
                AudioSource.PlayClipAtPoint(item.audioExtra, Camera.main.transform.position);

            yield return new WaitForSeconds(item.time);
        }
    }
}
