﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ResultsPanel : MonoBehaviour
{
    public Image[] ui_colors;
    public TMPro.TextMeshProUGUI[] ui_scores;

    public Image ui_flanders;

    public GameObject oddPanel;
    public GameObject evenPanel;

    public Image[] first;
    public Image[] second;
    public Image[] third;
    public Image[] forth;

    void RefreshPanel()
    {
        var sortedPlayers = GameStatus.instance.score
            .Select((p, i) => new KeyValuePair<int, GameStatus.Player>(i, p))
            .OrderByDescending(x => x.Value.score)
            .Select(x => x.Key)
            .ToArray();

        for(int i=0; i<4; i++)
        {
            ui_colors[i].color = HUDPlayerScore.GetPlayerColor((GameStatus.PlayerType)sortedPlayers[i]);
            ui_scores[i].text = GameStatus.instance.score[sortedPlayers[i]].score.ToString();
        }

        int maxScore = GameStatus.instance.score[sortedPlayers[0]].score;
        var winners = sortedPlayers.Where(p => GameStatus.instance.score[p].score == maxScore).ToArray();

        oddPanel.SetActive(winners.Length % 2 == 1);
        evenPanel.SetActive(winners.Length % 2 == 0);

        first.ForEach(i => i.color = HUDPlayerScore.GetPlayerColor((GameStatus.PlayerType)sortedPlayers[0]));
        second.ForEach(i => i.color = HUDPlayerScore.GetPlayerColor((GameStatus.PlayerType)sortedPlayers[1]));
        third.ForEach(i => i.color = HUDPlayerScore.GetPlayerColor((GameStatus.PlayerType)sortedPlayers[2]));
        forth.ForEach(i => i.color = HUDPlayerScore.GetPlayerColor((GameStatus.PlayerType)sortedPlayers[3]));

        ui_flanders.gameObject.SetActive(GameStatus.instance.score.Select(x => x.score).Distinct().Count() != GameStatus.instance.score.Length);
        first.ForEach(i => i.gameObject.SetActive(winners.Length >= 1));
        second.ForEach(i => i.gameObject.SetActive(winners.Length >= 2));
        third.ForEach(i => i.gameObject.SetActive(winners.Length >= 3));
        forth.ForEach(i => i.gameObject.SetActive(winners.Length >= 4));
    }

    private void OnEnable()
    {
        FxManager.PlaySound(66);
        RefreshPanel();
    }

    private int enterCount;

    private void Update()
    {
        if (enterCount >= 3)
            Application.Quit();

        if (Input.GetKeyDown(KeyCode.Return))
            enterCount++;
    }
}
