using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimeGridGenerator : MonoBehaviour
{
    public AnimeOption prefab;
    public Rect bounds;
    public int amount;
    public int rows;
    public int columns;

    public void Generate()
    {
        GetComponentsInChildren<AnimeOption>().ForEach(g => DestroyImmediate(g.gameObject));
        rows = Mathf.Max(rows, 1);
        columns = Mathf.Max(columns, 1);
        float width = bounds.width / columns;
        for (int i = 0; i < amount; i++)
        {
            var anime = Instantiate(prefab, transform);
            anime.index = i+1;
            anime.gameObject.name = $"AnimeOption({i})";
            anime.text = "";
            var t = anime.GetComponent<RectTransform>();
            t.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            t.anchoredPosition = new Vector2()
            {
                x = bounds.xMin + width * (i/rows) + width / 2,
                y = bounds.yMax - t.rect.height * (i%rows) - t.rect.height / 2
            };
        }
    }

    //private void OnEnable()
    //{
    //    Generate();
    //}
}
